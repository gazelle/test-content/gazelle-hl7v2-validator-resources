# IGAMT-conformance-profiles

This project is aimed to store the HL7 Conformance Profiles as they are produced by the NIST IGAMT tool.

## Structure of the directory

The project is to be structured as follow :
```
ACTOR_A_KEYWORD
        |_TRANSACTION_1_KEYWORD
                |_FULL_MSH_9_VALUE
        |_TRANSACTION_3_KEYWORD
                |_FULL_MSH_9_VALUE
ACTOR\_B\_KEYWORD
        |_TRANSACTION_5_KEYWORD
                |_FULL_MSH_9_VALUE
```

In each leaf directory, shall be stored (respect exactly the naming):

* Profile.xml, the file download from IGAMT representing the HL7 Conformance Profile
* ValueSets.xml, the file containing the values for semantic validation
* Constraints.xml (if predicate constraints have been defined)
* Display.html, the HTML view of the HL7 conformance profile (Profile Style is preferred)

## Download content from IGAMT

When logged in IGAMT, from the edition or view page of the Implementation Guide (IG), use the Export menu.

To export the HL7 Conformance Profile, use the "NIST Validation Format", you will download a ZIP archive containing the Profile.xml, ValueSets.xml, and Constraints.xml files.


