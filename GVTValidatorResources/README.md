# GVT Validation Resources 

This directory shall contain GVT jar to perform validation with IGAMT Conformance Profiles. 

It shall be named : _gvt-validation-jar-jar-with-dependencies.jar_

To perform  hl7v2 validation, Gazelle HL7 Validator need the __gvt-validation-jar-jar-with-dependencies.jar__
The jar is available in our Nexus repository. To get it on Nexus browse [here](https://gazelle.ihe.net/nexus/) and download the latest version.
