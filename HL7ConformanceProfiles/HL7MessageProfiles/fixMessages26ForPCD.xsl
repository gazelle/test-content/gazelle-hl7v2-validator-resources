<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0">
    <xsl:output indent="yes" method="xml"/>   
    
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    <xsl:template match="text()">
        <xsl:if test="parent::node()[name() = 'Reference' ] 
            or parent::node()[name() = 'ImpNote'] 
            or parent::node()[name() = 'Encoding']
            or parent::node()[name() = 'Description']
            or parent::node()[name() = 'Predicate']
            or parent::node()[name() = 'Purpose']">
            <xsl:copy-of select="."/>
        </xsl:if>
    </xsl:template>
    <xsl:template match="node()[name() = 'SubComponent']">
        <xsl:choose>
            <xsl:when test="@Datatype = 'CE'">
                <xsl:element name="SubComponent">
                    <xsl:copy-of select="@Name"/>
                    <xsl:copy-of select="@Length"/>
                    <xsl:copy-of select="@Usage"/>
                    <xsl:copy-of select="@Table"/>
                    <xsl:copy-of select="@ConstantValue"/>
                    <xsl:attribute name="Datatype">CWE</xsl:attribute>
                    <xsl:for-each select="child::node()">
                        <xsl:if test="name() != null">
                            <xsl:copy>
                                <xsl:apply-templates select="@*|node()"/>
                            </xsl:copy>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:element>
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy>
                    <xsl:apply-templates select="@*|node()"/>
                </xsl:copy>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="node()[name() = 'Component']">  
        <xsl:choose>
            <xsl:when test="@ItemNo = '00023'">
                <!-- This element is still of datatype CE, do not modify it -->
                <xsl:copy>
                    <xsl:apply-templates select="@*|node()"/>
                </xsl:copy>
            </xsl:when>
            <xsl:when test="@Datatype = 'CE'">
                <!-- replace datatype CE by CWE -->
                <xsl:comment>AGB: Fix datatype to CWE</xsl:comment>
                <xsl:element name="Component">
                    <xsl:copy-of select="@Name"/>
                    <xsl:copy-of select="@Usage"/>
                    <xsl:attribute name="Datatype">CWE</xsl:attribute>
                    <xsl:copy-of select="@Length"/>
                    <xsl:copy-of select="@ItemNo"/>
                    <xsl:copy-of select="@Max"/>
                    <xsl:copy-of select="@Min"/>
                    <xsl:for-each select="child::node()">
                        <xsl:if test="name() != null and name() != 'SubComponent'">
                            <xsl:copy>
                                <xsl:apply-templates select="@*|node()"/>
                            </xsl:copy>
                        </xsl:if>
                    </xsl:for-each>
                    <xsl:element name="SubComponent">
                        <xsl:attribute name="Name">identifier</xsl:attribute>
                        <xsl:attribute name="Usage">RE</xsl:attribute>
                        <xsl:attribute name="Datatype">ST</xsl:attribute>
                        <xsl:attribute name="Length">40</xsl:attribute>
                    </xsl:element>
                    <xsl:element name="SubComponent">
                        <xsl:attribute name="Name">text</xsl:attribute>
                        <xsl:attribute name="Usage">R</xsl:attribute>
                        <xsl:attribute name="Datatype">ST</xsl:attribute>
                        <xsl:attribute name="Length">199</xsl:attribute>
                    </xsl:element>
                    <xsl:element name="SubComponent">
                        <xsl:attribute name="Name">name of coding system</xsl:attribute>
                        <xsl:attribute name="Usage">RE</xsl:attribute>
                        <xsl:attribute name="Datatype">ID</xsl:attribute>
                        <xsl:attribute name="Length">20</xsl:attribute>
                    </xsl:element>
                    <xsl:element name="SubComponent">
                        <xsl:attribute name="Name">alternate identifier</xsl:attribute>
                        <xsl:attribute name="Usage">RE</xsl:attribute>
                        <xsl:attribute name="Datatype">ST</xsl:attribute>
                        <xsl:attribute name="Length">20</xsl:attribute>
                    </xsl:element>
                    <xsl:element name="SubComponent">
                        <xsl:attribute name="Name">alternate text</xsl:attribute>
                        <xsl:attribute name="Usage">RE</xsl:attribute>
                        <xsl:attribute name="Datatype">ST</xsl:attribute>
                        <xsl:attribute name="Length">199</xsl:attribute>
                    </xsl:element>
                    <xsl:element name="SubComponent">
                        <xsl:attribute name="Name">name of alternate coding system</xsl:attribute>
                        <xsl:attribute name="Usage">C</xsl:attribute>
                        <xsl:attribute name="Datatype">ID</xsl:attribute>
                        <xsl:attribute name="Length">20</xsl:attribute>
                    </xsl:element>
                    <xsl:element name="SubComponent">
                        <xsl:attribute name="Name">coding system version id</xsl:attribute>
                        <xsl:attribute name="Usage">C</xsl:attribute>
                        <xsl:attribute name="Datatype">ST</xsl:attribute>
                        <xsl:attribute name="Length">10</xsl:attribute>
                    </xsl:element>
                    <xsl:element name="SubComponent">
                        <xsl:attribute name="Name">alternate coding system version id</xsl:attribute>
                        <xsl:attribute name="Usage">O</xsl:attribute>
                        <xsl:attribute name="Datatype">ST</xsl:attribute>
                        <xsl:attribute name="Length">10</xsl:attribute>
                    </xsl:element>
                    <xsl:element name="SubComponent">
                        <xsl:attribute name="Name">original text</xsl:attribute>
                        <xsl:attribute name="Usage">O</xsl:attribute>
                        <xsl:attribute name="Datatype">ST</xsl:attribute>
                        <xsl:attribute name="Length">199</xsl:attribute>
                    </xsl:element>
                </xsl:element>
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy>
                    <xsl:apply-templates select="@*|node()"/>
                </xsl:copy>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>  
    
    <xsl:template match="node()[name() = 'Field']">
        <xsl:choose>
            <xsl:when test="@Datatype = 'CE'">
                <xsl:element name="Field">
                    <xsl:copy-of select="@Name"/>
                    <xsl:copy-of select="@Usage"/>
                    <xsl:attribute name="Datatype">CWE</xsl:attribute>
                    <xsl:copy-of select="@Length"/>
                    <xsl:copy-of select="@ItemNo"/>
                    <xsl:copy-of select="@Max"/>
                    <xsl:copy-of select="@Min"/>
                    <xsl:copy-of select="@Table"/>
                    <xsl:copy-of select="@ConstantValue"/>
                    <xsl:for-each select="child::node()">
                        <xsl:if test="name() != null and name() != 'Component'">
                            <xsl:copy>
                                <xsl:apply-templates select="@*|node()"/>
                            </xsl:copy>
                        </xsl:if>
                    </xsl:for-each>
                    <xsl:element name="Component">
                        <xsl:attribute name="Name">identifier</xsl:attribute>
                        <xsl:attribute name="Usage">RE</xsl:attribute>
                        <xsl:attribute name="Datatype">ST</xsl:attribute>
                        <xsl:attribute name="Length">40</xsl:attribute>
                    </xsl:element>
                    <xsl:element name="Component">
                        <xsl:attribute name="Name">text</xsl:attribute>
                        <xsl:attribute name="Usage">R</xsl:attribute>
                        <xsl:attribute name="Datatype">ST</xsl:attribute>
                        <xsl:attribute name="Length">199</xsl:attribute>
                    </xsl:element>
                    <xsl:element name="Component">
                        <xsl:attribute name="Name">name of coding system</xsl:attribute>
                        <xsl:attribute name="Usage">RE</xsl:attribute>
                        <xsl:attribute name="Datatype">ID</xsl:attribute>
                        <xsl:attribute name="Length">20</xsl:attribute>
                    </xsl:element>
                    <xsl:element name="Component">
                        <xsl:attribute name="Name">alternate identifier</xsl:attribute>
                        <xsl:attribute name="Usage">RE</xsl:attribute>
                        <xsl:attribute name="Datatype">ST</xsl:attribute>
                        <xsl:attribute name="Length">20</xsl:attribute>
                    </xsl:element>
                    <xsl:element name="Component">
                        <xsl:attribute name="Name">alternate text</xsl:attribute>
                        <xsl:attribute name="Usage">RE</xsl:attribute>
                        <xsl:attribute name="Datatype">ST</xsl:attribute>
                        <xsl:attribute name="Length">199</xsl:attribute>
                    </xsl:element>
                    <xsl:element name="Component">
                        <xsl:attribute name="Name">name of alternate coding system</xsl:attribute>
                        <xsl:attribute name="Usage">C</xsl:attribute>
                        <xsl:attribute name="Datatype">ID</xsl:attribute>
                        <xsl:attribute name="Length">20</xsl:attribute>
                    </xsl:element>
                    <xsl:element name="Component">
                        <xsl:attribute name="Name">coding system version id</xsl:attribute>
                        <xsl:attribute name="Usage">C</xsl:attribute>
                        <xsl:attribute name="Datatype">ST</xsl:attribute>
                        <xsl:attribute name="Length">10</xsl:attribute>
                    </xsl:element>
                    <xsl:element name="Component">
                        <xsl:attribute name="Name">alternate coding system version id</xsl:attribute>
                        <xsl:attribute name="Usage">O</xsl:attribute>
                        <xsl:attribute name="Datatype">ST</xsl:attribute>
                        <xsl:attribute name="Length">10</xsl:attribute>
                    </xsl:element>
                    <xsl:element name="Component">
                        <xsl:attribute name="Name">original text</xsl:attribute>
                        <xsl:attribute name="Usage">O</xsl:attribute>
                        <xsl:attribute name="Datatype">ST</xsl:attribute>
                        <xsl:attribute name="Length">199</xsl:attribute>
                    </xsl:element>
                </xsl:element>
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy>
                    <xsl:apply-templates select="@*|node()"/>
                </xsl:copy>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="comment()">
        <xsl:comment><xsl:value-of select="."/></xsl:comment>
    </xsl:template>
</xsl:stylesheet>