<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0">
	<xsl:output indent="yes" method="xml"/>   
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="text()">
		<xsl:if test="parent::node()[name() = 'Reference' ] 
			or parent::node()[name() = 'ImpNote'] 
			or parent::node()[name() = 'Encoding']
			or parent::node()[name() = 'Description']
			or parent::node()[name() = 'Predicate']
			or parent::node()[name() = 'Purpose']">
			<xsl:copy-of select="."/>
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="node()[name() = 'Field']">
		<xsl:choose>
			<xsl:when test="@Datatype = 'XCN'">
				<xsl:element name="Field">
					<xsl:copy-of select="@Name"/>
					<xsl:copy-of select="@Usage"/>
					<xsl:copy-of select="@Datatype"/>
					<xsl:copy-of select="@Length"/>
					<xsl:copy-of select="@ItemNo"/>
					<xsl:copy-of select="@Max"/>
					<xsl:copy-of select="@Min"/>
					<xsl:copy-of select="@Table"/>
					<xsl:copy-of select="@ConstantValue"/>
					<xsl:comment>ABE: fix XCN-9 usage, was X shall be O</xsl:comment>
					<xsl:for-each select="child::node()">
						<xsl:choose>
						<xsl:when test="name() != null and name() != 'Component'">
							<xsl:copy>
								<xsl:apply-templates select="@*|node()"/>
							</xsl:copy>
						</xsl:when>
						<xsl:when test="@Name = 'assigning authority'">
							<xsl:element name="Component">
								<xsl:copy-of select="@Name"/>
								<xsl:attribute name="Usage">O</xsl:attribute>
								<xsl:copy-of select="@Datatype"/>
								<xsl:copy-of select="@Length"/>
								<xsl:copy-of select="@Table"/>
								<xsl:copy-of select="@ConstantValue"/>
								<xsl:for-each select="child::node()">
									<xsl:copy>
										<xsl:apply-templates select="@*|node()"/>
									</xsl:copy>
								</xsl:for-each>
							</xsl:element>
						</xsl:when>
						<xsl:otherwise>
							<xsl:copy>
								<xsl:apply-templates select="@*|node()"/>
							</xsl:copy>
						</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy>
					<xsl:apply-templates select="@*|node()"/>
				</xsl:copy>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="comment()">
		<xsl:comment><xsl:value-of select="."/></xsl:comment>
	</xsl:template>
</xsl:stylesheet>