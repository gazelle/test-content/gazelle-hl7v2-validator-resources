# Gazelle HL7V2 Validator Resources

This project is aimed to store the Gazelle HL7V2 Validator resources : 
* GVT resources to perform validation for IGAMT Conformance Profiles ; 
* HL7 Conformance Profiles (old profiles) to perform Validation using Hapi Based Validator ; 
* IGAMT Conformance Profiles to perform validation using GVT Tool; 

 The master contains the HL7 Conformance profiles which match the IHE specifications. 
 For each regional project which specifies extensions to IHE profiles or other HL7v2 based messages, __a new branch shall be created__.
 
 
 Project shall be structured as follows : 
 ```
 gazelle-hl7v2-validator-resources
         |GVTValidatorResources
                 |gvt-validation-jar-jar-with-dependencies.jar
         |HL7ConformanceProfile
                 |HL7MessageProfiles
                        |ProfilesPerOid
                 |HL7Tables
                        |TablesPerOid
         |IGAMTConformanceProfile
                 |see_structure
                 
 ```
 